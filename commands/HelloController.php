<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Faker\Factory;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionGoodGenerate()
    {
        for($i = 0; $i < 996; $i++)
        {

            $goods[] = [

                (substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5)),
                (substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 10)),
//                rand(0, 1),
//                $faker->unixTime()
            ];
        }
        Yii::$app->db->createCommand()->batchInsert('goods', ['kod', 'name'], $goods)->execute();
        die('Data generation is complete!');
    }

    public function actionIncomeGenerate()
    {
        $text = Factory::create();
        $income=[];
        for($i = 0; $i < 500; $i++)
        {

            $income[] = [

                $text->date(),
//                (substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 10)),
                rand(1, 500),
//                $faker->unixTime()
            ];
        }

                Yii::$app->db->createCommand()->batchInsert('income', ['date', 'number'], $income)->execute();
        die('Data generation is complete!');
    }

    public function actionSaleGenerate()
    {
        $text = Factory::create();
        $sale=[];
        for($i = 0; $i < 2; $i++)
        {

            $sale[] = [

                $text->date(),
//                (substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 10)),
                rand(1, 500),
//                $faker->unixTime()
            ];
        }

                Yii::$app->db->createCommand()->batchInsert('sale', ['date', 'number'], $sale)->execute();
        die('Data generation is complete!');
    }

    public function actionIncomeGoodGenerate()
    {
        $k = 0;
        for($i = 0; $i < 1000; $i++)
        {
            $incomeGood=[];
            for ($j = 0; $j < 1000; $j++){
                $incomeGood[] = [
                    rand(5, 504), // income_id
                    rand(1, 1000), // good_id
                    rand(1, 1000), // amount
                    rand(100, 1000), // cost
                    rand(0, 1), // cost
                    date("2021-m-d h:i:s"), // created_at
                    date("2021-m-d h:i:s"), // updated_at
                ];
            }
            Yii::$app->db->createCommand()->batchInsert('income_good', ['income_id', 'good_id','amount','cost','status','updated_at','created_at'], $incomeGood)->execute();
            $k += 1000;
            echo $k.PHP_EOL;
            unset($incomeGood);
        }
//print_var($incomeGood);
        die('Data generation is complete!');
    }

    public function actionSaleGoodGenerate()
    {
        $k = 0;
        for($i = 0; $i < 1000; $i++)
        {
            $saleGood=[];
            for ($j = 0; $j < 1000; $j++){
                $saleGood[] = [
                    rand(4, 504), // sale_id
                    rand(1, 1000), // good_id
                    rand(1, 1000), // amount
                    rand(100, 1000), // cost
                    rand(0, 1), // cost
                    date("2021-m-d h:i:s"), // created_at
                    date("2021-m-d h:i:s"), // updated_at
                ];
            }
            Yii::$app->db->createCommand()->batchInsert('sale_good', ['sale_id', 'good_id','amount','cost','status','updated_at','created_at'], $saleGood)->execute();
            $k += 1000;
            echo $k.PHP_EOL;
            unset($saleGood);
        }
//print_var($incomeGood);
        die('Data generation is complete!');
    }
}
