<?php


namespace app\helpers;


class FormatHelper
{
    public static function nf($decimal)
    {
        return number_format($decimal,'0','.',' ');
    }
}