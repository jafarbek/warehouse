<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IncomeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="income-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'start')->input('date') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'end')->input('date') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'number') ?>
        </div>
        <br><div class="col-md-3">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Reset'), \yii\helpers\Url::to(['income/index']), ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
