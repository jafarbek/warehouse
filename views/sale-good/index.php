<?php

use app\models\Good;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeGoodSearch */
/* @var $sale \app\models\Sale */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = "Sale N:".$sale->number. " ".$sale->date;
$this->params['breadcrumbs'][] = $this->title;

?>
<?php $form = ActiveForm::begin(['id' => 'form-sale-good','enableAjaxValidation' => true]); ?>
    <div class="sale-good-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <div class="income-good-form">


            <div class="row">
                <?= $form->field($model, 'sale_id')->hiddenInput(['value'=>$sale->id])->label(false) ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'good_id')->dropDownList(Good::getRemainList()??[],['prompt'=>Yii::t('app',"Tanlang...")]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'amount')->textInput(['onchange'=>"calculate()"]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'cost')->textInput(['onchange'=>"calculate()"]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'summery')->textInput(['readonly'=>true]) ?>
                </div>
                <br><div class="col-md-3">
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>



        </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                [
                    'attribute' => 'good_id',
                    'label' => Yii::t('app','Name'),
                    'value' => function($model){
//                        return $model->good_id;
                        return $model->good->name;
                    }
                ],
                'amount',
                'cost',
                [
                    'attribute' => 'summery',
                    'value' => function($model){
                        return $model->cost * $model->amount;
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>


    </div>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
    function calculate() {
        let amount = $("#salegood-amount").val();
        let cost = $("#salegood-cost").val();
        $("#salegood-summery").val(amount*cost);
    }
JS;
$this->registerJs($js,View::POS_BEGIN);