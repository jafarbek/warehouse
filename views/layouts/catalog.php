<?php

/* @var $this \yii\web\View */
/* @var $content string */
$good_id = isset($this->context->actionParams['good_id']) ? $this->context->actionParams['good_id'] : null;
$action = \Yii::$app->controller->id."/".\Yii::$app->controller->action->id;
?>

<?php $this->beginContent('@app/views/layouts/layout.php')?>
<div class="row">
<!-- ФРАГМЕНТНИ КЕШЛАШ -->
    <div class="col-lg-2 col-md-2 col-sm-12">
        <?php if ($this->beginCache("side-bar-widget",
            [
                'dependency'=> [
                    'class' => \yii\caching\TagDependency::class,
                    'tags' => [\app\repositories\IncomeGoodRepository::allTagDependency(), \app\repositories\SaleGoodRepository::allTagDependency()]
                ],
                'duration'=>3600,
                'variations'=> $action.$good_id
        ]
        )) :?>
        <?= \app\widgets\SideBarWidget::widget([
            'good' => $good_id,
            'action' => $action
        ]);
            ?>
        <?php $this->endCache(); endif; ?>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-12">
        <?= $content ?>
    </div>
</div>
<?php $this->endContent()?>