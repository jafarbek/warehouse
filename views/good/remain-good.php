<?php



/* @var $this \yii\web\View */
/* @var $remainGood \app\models\Remain|null */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('app', "Omborda qolgan {$remainGood['name']}");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remain-good-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $remainGood,
        'attributes' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('app','Name'),
                'value' => function($model){
                    return $model['name'];
                }
            ],
            [
                'attribute' => 'incomeTotal',
                'label' => Yii::t('app','Jami kirim'),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['incomeTotal'])." so'm";
                }
            ],
            [
                'attribute' => 'income_avg',
                'label' => Yii::t('app',"Kirim o'rtacha"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['income_avg'])." so'm";
                }
            ],
            [
                'attribute' => 'income_summery',
                'label' => Yii::t('app',"Kirim Summa"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['income_summery'])." so'm";
                }
            ],
            [
                'attribute' => 'saleTotal',
                'label' => Yii::t('app',"Jami chiqim"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['saleTotal'])." so'm";
                }
            ],
            [
                'attribute' => 'sale_avg',
                'label' => Yii::t('app',"Chiqim o'rtacha"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['sale_avg'])." so'm";
                }
            ],
            [
                'attribute' => 'sale_summery',
                'label' => Yii::t('app',"Chiqim Summa"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['sale_summery'])." so'm";
                }
            ],
            [
                'attribute' => 'remain',
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['remain'])." so'm";
                }
            ],
            [
                'attribute' => 'earn',
                'label' => Yii::t('app',"O'rtacha daromad"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['earn'])." so'm";
                }
            ],
        ],
    ]) ?>

</div>
