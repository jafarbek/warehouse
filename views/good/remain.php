<?php

use app\models\Good;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeGoodSearch */
/* @var $income \app\models\Income */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Remain";
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="remain">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                    'attribute' => 'name',
                    'label' => Yii::t('app','Name'),
                    'value' => function($model){
                        return $model['name'];
                    }
            ],
            [
                    'attribute' => 'incomeTotal',
                    'label' => Yii::t('app','Jami kirim'),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['incomeTotal']);
                    }
            ],
            [
                    'attribute' => 'income_avg',
                    'label' => Yii::t('app',"Kirim o'rtacha narx"),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['income_avg'])." so'm";
                    }
            ],
            [
                    'attribute' => 'income_summery',
                    'label' => Yii::t('app',"Kirim Summa"),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['income_summery'])." so'm";
                    }
            ],
            [
                    'attribute' => 'saleTotal',
                    'label' => Yii::t('app',"Jami chiqim"),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['saleTotal']);
                    }
            ],
            [
                    'attribute' => 'sale_avg',
                    'label' => Yii::t('app',"Chiqim o'rtacha"),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['sale_avg'])." so'm";
                    }
            ],
            [
                    'attribute' => 'sale_summery',
                    'label' => Yii::t('app',"Chiqim Summa"),
                    'value' => function($model){
                        return \app\helpers\FormatHelper::nf($model['sale_summery'])." so'm";
                    }
            ],
            [
                'attribute' => 'remain',
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['remain']);
                }
            ],
            [
                'attribute' => 'earn',
                'label' => Yii::t('app',"O'rtacha daromad"),
                'value' => function($model){
                    return \app\helpers\FormatHelper::nf($model['earn'])." so'm";
                }
            ],
        ],
    ]); ?>


</div>
<?php
$js = <<<JS
    function calculate() {
        let amount = $("#incomegood-amount").val();
        let cost = $("#incomegood-cost").val();
        $("#incomegood-summery").val(amount*cost);
    }
JS;
$this->registerJs($js,View::POS_BEGIN);