<?php

use app\models\Good;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeGoodSearch */
/* @var $income \app\models\Income */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','List of arrived goods');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="income-good-list">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                    'attribute' => 'good_id',
                    'value' => function($model){
                        return $model->good->name;
                    }
            ],
            'amount',
            'cost',
            [
                'attribute' => 'summery',
                'value' => function($model){
                    return $model->cost * $model->amount;
                }
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
<?php
$js = <<<JS
    function calculate() {
        let amount = $("#incomegood-amount").val();
        let cost = $("#incomegood-cost").val();
        $("#incomegood-summery").val(amount*cost);
    }
JS;
$this->registerJs($js,View::POS_BEGIN);