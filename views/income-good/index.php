<?php

use app\models\Good;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeGoodSearch */
/* @var $income \app\models\Income */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Income N:".$income->number. " ".$income->date;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="income-good-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="income-good-form">
        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
                <?= $form->field($model, 'income_id')->hiddenInput(['value'=>$income->id])->label(false) ?>
            <div class="col-md-3">
                <?= $form->field($model, 'good_id')->dropDownList(Good::getList()??[],['prompt'=>Yii::t('app',"Tanlang...")]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'amount')->textInput(['onchange'=>"calculate()"]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'cost')->textInput(['onchange'=>"calculate()"]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'summery')->textInput(['readonly'=>true]) ?>
            </div>
            <br><div class="col-md-3">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Add'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                    'attribute' => 'good_id',
                    'label' => Yii::t('app','Name'),
                    'value' => function($model){
                        return $model->good->name;
                    }
            ],
            'amount',
            'cost',
            [
                'attribute' => 'summery',
                'value' => function($model){
                    return $model->cost * $model->amount;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
<?php
$js = <<<JS
    function calculate() {
        let amount = $("#incomegood-amount").val();
        let cost = $("#incomegood-cost").val();
        $("#incomegood-summery").val(amount*cost);
    }
JS;
$this->registerJs($js,View::POS_BEGIN);