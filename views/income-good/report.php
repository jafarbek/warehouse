<?php

use app\helpers\FormatHelper;
use app\helpers\TotalHelper;
use app\models\Good;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeGoodSearch */
/* @var $income \app\models\Income */
/* @var $good \app\models\Good */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t("Omborga kirim bo'lgan {$dataProvider->models[0]->good->name} hisoboti");
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="income-good-list">

        <h1><?= Html::encode($this->title) ?></h1>
        <strong><p class="text-center"><?=  "O'rtacha narh  (".FormatHelper::nf(
                TotalHelper::getTotal($dataProvider->models,'cost') /
                $dataProvider->count)." so'm)" ?></p></strong>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'showFooter'=>TRUE,
            'footerRowOptions'=>['style'=>'font-weight:bold;text-decoration: underline; background:grey;color:white'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                [
                    'attribute' => 'good_id',
                    'label' => t("Tovar nomi"),
                    'value' => function($model){
                        return $model->good->name;
                    },
                    'footer' =>"Jami"
                ],
                [
                    'attribute' => 'date',
                    'label' => t("Oy nomi"),
                    'value' => function($model){
                        return $model->income->date;
                    },
                ],
                [
                    'attribute' => 'amount',
                    'footer' =>FormatHelper::nf(TotalHelper::getTotal($dataProvider->models,'amount')),
                    'label' => t("Miqdori")
                ],
                [
                    'attribute' => 'cost',
                    'value' => function($model){
                        return FormatHelper::nf($model->cost);
                    },
                    'label' => t("Narhi")
                ],
                [
                    'attribute' => 'summery',
                    'value' => function($model){
                        return FormatHelper::nf($model->summery);
                    },
                    'footer' =>FormatHelper::nf(TotalHelper::getTotal($dataProvider->models,'summery')),
                    'label' => t("JAMI")
                ],

//            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>


    </div>