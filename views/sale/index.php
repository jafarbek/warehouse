<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sales');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sale-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
        <div class="pull-left">
            <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </div>
        <div class="pull-left">
            <?= Html::a(Yii::t('app', 'Sales list'), ['sale-good/list'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <br>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'date',
            [
                'attribute' => 'number',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->number,
                        \yii\helpers\Url::to(['sale-good/index', 'id' => $model->id]), [
                            'title' => Yii::t('app', $model->number),
//                            'class' => 'btn btn-warning modal-button'
                        ]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'header' => Yii::t('app', 'Действии'),
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'btn btn-warning modal-button'
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-danger modal-button',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
