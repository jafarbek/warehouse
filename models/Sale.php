<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sale".
 *
 * @property int $id
 * @property string|null $date
 * @property string|null $number
 *
 * @property SaleGood[] $saleGoods
 */
class Sale extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale';
    }

    public function behaviors()
    {
        return [
            [
                'class'=>FileBehavior::class,
                'attr' => 'number',
                'attr2' => 'date'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date','number'], 'required'],
            [['number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'number' => Yii::t('app', 'Number'),
        ];
    }

    /**
     * Gets query for [[SaleGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSaleGoods()
    {
        return $this->hasMany(SaleGood::className(), ['sale_id' => 'id']);
    }
}
