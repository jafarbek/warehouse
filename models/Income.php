<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "income".
 *
 * @property int $id
 * @property string|null $date
 * @property string|null $number
 *
 * @property IncomeGood[] $incomeGoods
 */
class Income extends ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class'=>FileBehavior::class,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'income';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date','number'], 'required'],
            [['number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'number' => Yii::t('app', 'Number'),
        ];
    }

    /**
     * Gets query for [[IncomeGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeGoods()
    {
        return $this->hasMany(IncomeGood::className(), ['income_id' => 'id']);
    }

//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//            echo "Saqlanyapti";
//            return true;
//        }
//        return false;
//    }
}
