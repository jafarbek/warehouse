<?php

namespace app\models;

use app\services\RemainReportService;
use phpDocumentor\Reflection\Types\Array_;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property string|null $kod
 * @property string|null $name
 *
 * @property IncomeGood[] $incomeGoods
 * @property Remain[] $remain
 * @property SaleGood[] $saleGoods
 */
class Good extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kod', 'name'], 'required'],
            [['kod', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kod' => Yii::t('app', 'Kod'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * Gets query for [[IncomeGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeGoods()
    {
        return $this->hasMany(IncomeGood::className(), ['good_id' => 'id']);
    }

    public function getRemain()
    {
        return $this->hasOne(Remain::className(), ['good_id' => 'id']);
    }

    public static function getList()
    {
        return  ArrayHelper::map(self::find()->all(),'id','name');
    }

    public static function getRemainList(): array
    {
        $goods = (new RemainReportService())->getTitleAndRemainFromCache();
        $goods = array_map(function ($value){
            return $value['name'].' ------- '.$value['remain'];
        },$goods);

        if (empty($goods))
            return [];

        return $goods;
    }

    /**
     * Gets query for [[SaleGoods]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSaleGoods()
    {
        return $this->hasMany(SaleGood::className(), ['good_id' => 'id']);
    }
}
