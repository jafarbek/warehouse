<?php

namespace app\models;

use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SaleGood;

/**
 * SaleGoodSearch represents the model behind the search form of `app\models\SaleGood`.
 */
class SaleGoodSearch extends SaleGood
{
    public $summery;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sale_id', 'good_id', 'amount', 'cost'], 'integer'],
            [['summery'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id=false)
    {
        $query = SaleGood::find();

        if ($id) $query->where(['sale_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sale_id' => $this->sale_id,
            'good_id' => $this->good_id,
            'amount' => $this->amount,
            'cost' => $this->cost,
        ]);

        return $dataProvider;
    }

    public function searchGood($params, $good_id=false)
    {
        $query = SaleGood::find();

        if ($good_id) $query->where(['good_id'=>$good_id])->with(['good','sale']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sale_id' => $this->sale_id,
            'good_id' => $this->good_id,
            'amount' => $this->amount,
            'cost' => $this->cost,
        ]);

        return $dataProvider;
    }
}
