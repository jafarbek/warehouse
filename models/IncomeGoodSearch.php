<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\IncomeGood;

/**
 * IncomeGoodSearch represents the model behind the search form of `app\models\IncomeGood`.
 */
class IncomeGoodSearch extends IncomeGood
{
    public $summery;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['summery','good_id',], 'safe'],
            [['id', 'income_id',  'amount', 'cost'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id=false)
    {
        $query = IncomeGood::find();

        if ($id) $query->where(['income_id'=>$id]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'income_id' => $this->income_id,
            'amount' => $this->amount,
            'cost' => $this->cost,
        ]);
        if ($this->good_id) {
            $query->joinWith(['good g'])->andWhere(['like','g.name',$this->good_id]);
        }

        return $dataProvider;
    }

    public function searchGood($params, $good_id=false)
    {
        $query = IncomeGood::find();

        if ($good_id) $query->where(['good_id'=>$good_id])->with(['good','income']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'income_id' => $this->income_id,
            'good_id' => $this->good_id,
            'amount' => $this->amount,
            'cost' => $this->cost,
        ]);

        return $dataProvider;
    }
}
