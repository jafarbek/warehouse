<?php

namespace app\models;

use base\components\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "sale_good".
 *
 * @property int $id
 * @property int|null $sale_id
 * @property int|null $good_id
 * @property int|null $amount
 * @property int|null $cost
 * @property int|null $summery
 *
 * @property Good $good
 * @property Sale $sale
 */
class SaleGood extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    public $summery;
    /**
     * {@inheritdoc}
     */

    public static function allCacheWithGood()
    {

    }

    public static function tableName()
    {
        return 'sale_good';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'good_id', 'amount', 'cost'], 'required'],
            [['sale_id', 'good_id', 'amount', 'cost'], 'integer'],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Good::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sale::className(), 'targetAttribute' => ['sale_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sale_id' => Yii::t('app', 'Sale ID'),
            'good_id' => Yii::t('app', 'Good ID'),
            'amount' => Yii::t('app', 'Amount'),
            'cost' => Yii::t('app', 'Cost'),
            'summery' => Yii::t('app', 'Сумма'),
        ];
    }

    /**
     * Gets query for [[Good]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }

    /**
     * Gets query for [[Sale]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sale::className(), ['id' => 'sale_id']);
    }

    public static function find()
    {
        return new SaleGoodQuery(get_called_class());
    }

    public function getSummery()
    {
        $this->summery = $this->amount * $this->cost;
    }
}
