<?php


namespace app\models;


use app\services\RemainReportService;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class RemainSearch extends Remain
{
    /**
     *
     * @property array|null $incomeTotal
     * @property int|null $soldTotal
     * @property int|null $remain
     */

    public $summery;
    public $remain;
    public $cost;

    public function rules()
    {
        return [
            [['good_id','cost','remain','summery'], 'safe'],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Good::className(), 'targetAttribute' => ['good_id' => 'id']],
        ];
    }

   public function search($params){
       $data = (new RemainReportService())->getReportFromCache();

       // add conditions that should always apply here

       $dataProvider = new ArrayDataProvider([
           'allModels' => $data,
           'pagination' => [
               'pageSize' => 10,
           ],
           'sort' => [
               'attributes' => ['name'],
           ],
           /*'sort' => [
               'attributes' => [
                   'good_id', 'summery','remain'
               ],
               'defaultOrder' => ['remain' => SORT_DESC]]*/
       ]);
       $this->load($params);

       if (!$this->validate()) {
           // uncomment the following line if you do not want to return any records when validation fails
           // $query->where('0=1');
           return $dataProvider;
       }

//       $query->andFilterWhere([
//           'income_id' => $this->income_id,
//           'good_id' => $this->good_id,
//           'amount' => $this->amount,
//           'cost' => $this->cost,
//       ]);
       return $dataProvider;
   }

}