<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "income_good".
 *
 * @property int $id
 * @property int|null $income_id
 * @property int|null $good_id
 * @property int|null $amount
 * @property int|null $cost
 * @property int|null $status
 * @property int|null $summery
 *
 * @property Good $good
 * @property Income $income
 * @property int $created_at [timestamp]
 * @property int $updated_at [timestamp]
 */
class IncomeGood extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'income_good';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['income_id', 'good_id', 'amount', 'cost','status'], 'integer'],
            [['income_id', 'good_id', 'amount', 'cost'], 'required'],
            [['summery'], 'safe'],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Good::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['income_id'], 'exist', 'skipOnError' => true, 'targetClass' => Income::className(), 'targetAttribute' => ['income_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'income_id' => Yii::t('app', 'Kirim hujjat nomi'),
            'good_id' => Yii::t('app', 'Mahsulot nomi'),
            'amount' => Yii::t('app', 'Miqdori'),
            'cost' => Yii::t('app', 'Narhi'),
            'summery' => Yii::t('app', 'Summasi'),
        ];
    }

    /**
     * Gets query for [[Good]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }

    /**
     * Gets query for [[Income]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncome()
    {
        return $this->hasOne(Income::className(), ['id' => 'income_id']);
    }

    public static function find()
    {
        return new IncomeGoodQuery(get_called_class());
    }

    public function getSummery()
    {
        return $this->amount * $this->cost;
    }
}
