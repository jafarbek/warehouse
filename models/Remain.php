<?php


namespace app\models;


use app\services\ReporterService;
use Yii;
use yii\db\ActiveQuery;

class Remain extends IncomeGood
{

    /**
     *
     * @property int|null $incomeTotal
     * @property $soldTotal
     * @property $total
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'income_id' => Yii::t('app', 'Kirim hujjat nomi'),
            'good_id' => Yii::t('app', 'Mahsulot nomi'),
            'amount' => Yii::t('app', 'Miqdori'),
            'cost' => Yii::t('app', 'O`rtacha narhi'),
            'summery' => Yii::t('app', 'Summasi'),
        ];
    }

    public function getIncomeTotal()
    {
        $amount=0;
        $cost=0;
        $incomes = static::find()->where(['good_id'=>$this->good_id])->all();
        foreach ($incomes as $income){
            $amount += $income['amount'];
            $cost += $income['cost'] * $income['amount'];
        }
        $cost = round($cost/$amount);
        return ['amount'=>$amount, 'cost'=>$cost];
    }

    public function getSoldTotal()
    {
        $amount=0;
        $soldProd = SaleGood::find()->where(['good_id'=>$this->good_id]);
        foreach ($soldProd->each() as $sold)
            $amount += $sold->amount;

        return $amount;
    }

    public function getTotal()
    {
        $total = $this->incomeTotal['amount'] - $this->soldTotal;
        return $total;
    }

}