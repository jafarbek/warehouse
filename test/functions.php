<?php
function print_var($array1,$array2=false){
    echo "<pre>";
    print_r($array1);
    echo "</pre>";
    if ($array2) {
        echo "<pre>";
        print_r($array2);
        echo "</pre>";
    }
    die();
}

function t($message){
    return Yii::t('app',$message);
}