<?php


namespace app\repositories;


use app\models\SaleGood;
use app\models\SaleGoodQuery;
use yii\caching\TagDependency;

class SaleGoodRepository
{
    public static function allTagDependency(): string
    {
        return "all_sale_goods";
    }
    
    private SaleGoodQuery $saleGoodQuery;

    public function __construct()
    {
        $this->saleGoodQuery = SaleGood::find();
    }

    public function getAll($asArray=true, $joinWithGood=true): array
    {
        $sales = $this->saleGoodQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $sales = $asArray
                ? $sales->leftJoin('goods','goods.id = sale_good.good_id')->indexBy(['good_id'])->groupBy('good_id')->allArray()
                : $sales->leftJoin('goods','goods.id = sale_good.good_id')->indexBy(['good_id'])->groupBy('good_id')->all();
        }else {
            $sales = $asArray
                ? $sales->indexBy(['good_id'])->groupBy('good_id')->allArray()
                : $sales->indexBy(['good_id'])->groupBy('good_id')->all();
        }
        return !empty($sales) ? $sales : [];
    }

    public function getAllFromCache($asArray=true, $joinWithGood=true): array
    {
        $sales = $this->saleGoodQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $sales = $asArray
                ? $sales->leftJoin('goods','goods.id = sale_good.good_id')
                    ->indexBy(['good_id'])
                    ->cache(0,new TagDependency(['tags'=>self::allTagDependency()]))
                    ->groupBy('good_id')
                    ->allArray()
                : $sales->leftJoin('goods','goods.id = sale_good.good_id')
                    ->indexBy(['good_id'])
                    ->groupBy('good_id')
                    ->cache(0)
                    ->all();
        }else {
            $sales = $asArray
                ? $sales->indexBy(['good_id'])->cache(0)->groupBy('good_id')->allArray()
                : $sales->indexBy(['good_id'])->cache(0)->groupBy('good_id')->all();
        }
        return !empty($sales) ? $sales : [];
    }

    public function getByGoodID($goodId,$asArray=true,$joinWithGood=true): array
    {
        $sales = $this->saleGoodQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $sales = $asArray
                ? $sales->whereGoodId($goodId)->leftJoin('goods','goods.id = sale_good.good_id')->oneArray()
                : $sales->whereGoodId($goodId)->leftJoin('goods','goods.id = sale_good.good_id')->one();
        }else {
            $sales = $asArray
                ? $sales->whereGoodId($goodId)->oneArray()
                : $sales->whereGoodId($goodId)->one();
        }

        return !empty($sales) ? $sales : [];
    }
}