<?php


namespace app\repositories;


use app\models\IncomeGood;
use app\models\IncomeGoodQuery;
use yii\base\BaseObject;
use yii\caching\TagDependency;

class IncomeGoodRepository
{
    private IncomeGoodQuery $incomesQuery;

    public function __construct()
    {
        $this->incomesQuery = IncomeGood::find();
    }

    public static function allTagDependency(): string
    {
        return "all_income_goods";
    }

    public function getAll($asArray=true, $joinWithGood=true): array
    {
        $incomes = $this->incomesQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $incomes = $asArray
                ? $incomes->leftJoin('goods','goods.id = income_good.good_id')->indexBy(['good_id'])->groupBy('good_id')->allArray()
                : $incomes->leftJoin('goods','goods.id = income_good.good_id')->indexBy(['good_id'])->groupBy('good_id')->all();
        }else {
            $incomes = $asArray
                ? $incomes->indexBy(['good_id'])->groupBy('good_id')->allArray()
                : $incomes->indexBy(['good_id'])->groupBy('good_id')->all();
        }
        return !empty($incomes) ? $incomes : [];
    }

    public function getAllFromCache($asArray=true, $joinWithGood=true): array
    {
        $incomes = $this->incomesQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $incomes = $asArray
                ? $incomes->leftJoin('goods','goods.id = income_good.good_id')
                    ->indexBy(['good_id'])
                    ->cache(0,new TagDependency(['tags'=>self::allTagDependency()]))
                    ->groupBy('good_id')
                    ->allArray()
                : $incomes->leftJoin('goods','goods.id = income_good.good_id')
                    ->indexBy(['good_id'])
                    ->groupBy('good_id')
                    ->cache(0)
                    ->all();
        }else {
            $incomes = $asArray
                ? $incomes->indexBy(['good_id'])->cache(0)->groupBy('good_id')->allArray()
                : $incomes->indexBy(['good_id'])->cache(0)->groupBy('good_id')->all();
        }
        return !empty($incomes) ? $incomes : [];
    }

    public function getByGoodID($goodId,$asArray=true,$joinWithGood=true): array
    {
        $incomes = $this->incomesQuery->selectSumAmountAndSummeryAndAvgCost();
        if ($joinWithGood) {
            $incomes = $asArray
                ? $incomes->whereGoodId($goodId)->leftJoin('goods','goods.id = income_good.good_id')->oneArray()
                : $incomes->whereGoodId($goodId)->leftJoin('goods','goods.id = income_good.good_id')->one();
        }else {
            $incomes = $asArray
                ? $incomes->whereGoodId($goodId)->oneArray()
                : $incomes->whereGoodId($goodId)->one();
        }

        return !empty($incomes) ? $incomes : [];
    }
}