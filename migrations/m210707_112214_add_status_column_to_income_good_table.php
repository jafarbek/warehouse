<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%income_good}}`.
 */
class m210707_112214_add_status_column_to_income_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%income_good}}', 'status', $this->smallInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%income_good}}', 'status');
    }
}
