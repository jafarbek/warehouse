<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sale_good}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%sale}}`
 * - `{{%goods}}`
 */
class m210215_172255_create_sale_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sale_good}}', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'good_id' => $this->integer(),
            'amount' => $this->integer(),
            'cost' => $this->integer(),
        ]);

        // creates index for column `sale_id`
        $this->createIndex(
            '{{%idx-sale_good-sale_id}}',
            '{{%sale_good}}',
            'sale_id'
        );

        // add foreign key for table `{{%sale}}`
        $this->addForeignKey(
            '{{%fk-sale_good-sale_id}}',
            '{{%sale_good}}',
            'sale_id',
            '{{%sale}}',
            'id',
            'CASCADE'
        );

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-sale_good-good_id}}',
            '{{%sale_good}}',
            'good_id'
        );

        // add foreign key for table `{{%goods}}`
        $this->addForeignKey(
            '{{%fk-sale_good-good_id}}',
            '{{%sale_good}}',
            'good_id',
            '{{%goods}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%sale}}`
        $this->dropForeignKey(
            '{{%fk-sale_good-sale_id}}',
            '{{%sale_good}}'
        );

        // drops index for column `sale_id`
        $this->dropIndex(
            '{{%idx-sale_good-sale_id}}',
            '{{%sale_good}}'
        );

        // drops foreign key for table `{{%goods}}`
        $this->dropForeignKey(
            '{{%fk-sale_good-good_id}}',
            '{{%sale_good}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-sale_good-good_id}}',
            '{{%sale_good}}'
        );

        $this->dropTable('{{%sale_good}}');
    }
}
