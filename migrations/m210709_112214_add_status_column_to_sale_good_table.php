<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%sale_good}}`.
 */
class m210709_112214_add_status_column_to_sale_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sale_good}}', 'status', $this->smallInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sale_good}}', 'status');
    }
}
