<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%income_good}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%income}}`
 * - `{{%goods}}`
 */
class m210215_171726_create_income_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%income_good}}', [
            'id' => $this->primaryKey(),
            'income_id' => $this->integer(),
            'good_id' => $this->integer(),
            'amount' => $this->integer(),
            'cost' => $this->integer(),
        ]);

        // creates index for column `income_id`
        $this->createIndex(
            '{{%idx-income_good-income_id}}',
            '{{%income_good}}',
            'income_id'
        );

        // add foreign key for table `{{%income}}`
        $this->addForeignKey(
            '{{%fk-income_good-income_id}}',
            '{{%income_good}}',
            'income_id',
            '{{%income}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-income_good-good_id}}',
            '{{%income_good}}',
            'good_id'
        );

        // add foreign key for table `{{%goods}}`
        $this->addForeignKey(
            '{{%fk-income_good-good_id}}',
            '{{%income_good}}',
            'good_id',
            '{{%goods}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%income}}`
        $this->dropForeignKey(
            '{{%fk-income_good-income_id}}',
            '{{%income_good}}'
        );

        // drops index for column `income_id`
        $this->dropIndex(
            '{{%idx-income_good-income_id}}',
            '{{%income_good}}'
        );

        // drops foreign key for table `{{%goods}}`
        $this->dropForeignKey(
            '{{%fk-income_good-good_id}}',
            '{{%income_good}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-income_good-good_id}}',
            '{{%income_good}}'
        );

        $this->dropTable('{{%income_good}}');
    }
}
