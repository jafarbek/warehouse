<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%income_good}}`.
 */
class m210707_112214_add_timestamps_column_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%income_good}}', 'created_at', $this->timestamp());
        $this->addColumn('{{%income_good}}', 'updated_at', $this->timestamp());
        $this->addColumn('{{%sale_good}}', 'created_at', $this->timestamp());
        $this->addColumn('{{%sale_good}}', 'updated_at', $this->timestamp());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%income_good}}', 'created_at');
        $this->dropColumn('{{%income_good}}', 'updated_at');
        $this->dropColumn('{{%sale_good}}', 'created_at');
        $this->dropColumn('{{%sale_good}}', 'updated_at');
    }
}
