<?php

namespace app\controllers;

use app\models\IncomeGood;
use app\models\Remain;
use app\models\RemainSearch;
use app\models\SaleGood;
use app\services\RemainReportService;
use Yii;
use app\models\Good;
use app\models\GoodSearch;
use yii\base\BaseObject;
use yii\caching\ChainedDependency;
use yii\caching\DbDependency;
use yii\data\ArrayDataProvider;
use yii\filters\HttpCache;
use yii\filters\PageCache;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GoodController implements the CRUD actions for Good model.
 *  * @property RemainReportService remainService

 */
class GoodController extends Controller
{
    private RemainReportService $remainService;

    public function __construct($id, $module, $config = [])
    {
        $this->remainService = new RemainReportService();
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
//            [
//                'class'=> PageCache::class,
//                'duration' => 3600,
//                'only' => ['remain-good'],
//                'dependency' => [
//                    'class'=> ChainedDependency::class,
//                    'dependencies' => [
//                        new DbDependency(['sql' => "SELECT MAX(updated_at) FROM ".IncomeGood::tableName()]),
//                        new DbDependency(['sql' => "SELECT MAX(updated_at) FROM ".SaleGood::tableName()]),
//                    ]
//
//                ],
//                'variations' => [
//                    Yii::$app->request->get('id')
//                ]
//            ]
//            [
//                'class' => HttpCache::class, // 304 response code
//                'only' => ['remain'],
//                'lastModified' => function () {
//                    $incomeMax = strtotime(IncomeGood::find()->max('updated_at'));
//                    $saleMax = strtotime(SaleGood::find()->max('updated_at'));
//                    return $incomeMax + $saleMax;
//                },
//            ],
//            [
//                'class' => HttpCache::class,
//                'only' => ['remain'],
//                'etagSeed' => function () {
//                    $incomeMax = IncomeGood::find()->max('updated_at');
//                    $saleMax = SaleGood::find()->max('updated_at');
//                    $serialize = serialize([
//                        Yii::$app->user->id,
//                        $incomeMax,
//                        $saleMax
//                    ]);
//                    return "$serialize";
//                },
//            ]
        ];
    }

    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRemain()
    {
//        $searchModel = new RemainSearch();
        $remain = $this->remainService->getReportFromCache();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $remain,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'attributes' => ['name'],
            ],
        ]);
        return $this->render('remain', [
//            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRemainGood($id)
    {
        $remainGood = $this->remainService->getByGoodId($id);

        return $this->render('remain-good', [
            'remainGood' => $remainGood,
        ]);
    }

    /**
     * Displays a single Good model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Good model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Good();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Good model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Good model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Good model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Good the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Good::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
