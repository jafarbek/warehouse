<?php

namespace app\controllers;

use app\models\Good;
use app\models\Income;
use app\repositories\IncomeGoodRepository;
use Yii;
use app\models\IncomeGood;
use app\models\IncomeGoodSearch;
use yii\caching\TagDependency;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IncomeGoodController implements the CRUD actions for IncomeGood model.
 */
class IncomeGoodController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IncomeGood models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new IncomeGoodSearch();
        $income = Income::findOne($id);
        $model = new IncomeGood();
        if ($model->load(Yii::$app->request->post())&&$model->save()){
            TagDependency::invalidate(Yii::$app->cache,IncomeGoodRepository::allTagDependency());
            return $this->refresh();
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'model'=> $model,
            'income'=> $income,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionList()
    {
        $searchModel = new IncomeGoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport($good_id)
    {
        $searchModel = new IncomeGoodSearch();
        $dataProvider = $searchModel->searchGood(Yii::$app->request->queryParams, $good_id);
        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single IncomeGood model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IncomeGood model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IncomeGood();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TagDependency::invalidate(Yii::$app->cache,IncomeGoodRepository::allTagDependency());
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IncomeGood model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TagDependency::invalidate(Yii::$app->cache,IncomeGoodRepository::allTagDependency());
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing IncomeGood model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $good = $this->findModel($id);
        $good->delete();
        return $this->redirect(['index','id'=>$good->income_id]);
    }

    /**
     * Finds the IncomeGood model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IncomeGood the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IncomeGood::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
