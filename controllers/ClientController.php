<?php

namespace app\controllers;

use Yii;

class ClientController extends \yii\web\Controller
{
    // Client Call
    //
    public function actionIndex()
    {
        $client = Yii::$app->siteApi;
        echo $client->getHello('Alex');
    }

}
