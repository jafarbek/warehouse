<?php


namespace app\controllers;


class EightyProblem
{
//    Ternar operatorda
//    1 - masala
    public function min(float $a, float $b, float $c): float
    {
        return $a<=$b && $a<=$c ? $a : ($b<=$a && $b<=$c ? $b : $c);
    }

    //    2 - masala
    public function kabisa(int $year): int
    {
        if ($year%4!==0 or ($year%100===0 and $year%400!==0)) {
            return 365;
        }
        return 366;
    }

    //    3 - masala
    public function problem3(int $x, int $y): string
    {
        if ($x>$y) {
            return  $x*$y*2 . '  ' .($x+$y)/2;
        }elseif ($x<$y){
            return ($x+$y)/2 . '  ' . ($x*$y)*2;
        }

        return $x.'   '.$y;
    }

    //    4 - masala
    public function problem4(int $day, int $month): string
    {
        $maxMonthCount = $this->monthCount($month);
        $nextMonth = $month+1;
        $monthZero = $month<10 ? "0": null;
        $dayZero = $day<10 ? "0": null;
        if ($maxMonthCount<$day){
            return "Bunday sana yo'q";
        }elseif ($month >12){
            return "Bunday oy yo'q";
        }

        if ($maxMonthCount===$day) {
            if ($month===12) {
                $nextMonth = 1;
            }
            return "01" . '.'.$monthZero.$nextMonth;
        }

        $day++;
        return  $dayZero.$day. '.' .$monthZero.$month;
    }

    private function monthCount($month): int
    {
        switch ($month){
            case 2: return 28;
            case 4:
            case 6:
            case 9:
            case 11: return 30;
            default : return 31;
        }
    }

    private function tubsonlikgaTekshirish($son){
        for($i = 2; $i <= sqrt($son); $i++) {
            if($son%$i == 0) {
                return false;
            }
        }

        return true;

    }

    //    5 - masala Mukammal sonlarni topish
    public function problem5(int $N)
    {
        $result = [];
//        $total = 0;
//        for ($i=1; $i<=$x; $i++){
//            for ($j=1; $j<=$i; $j++){
//              if ($i%$j===0 && $i!==$j) {
//                  $total += $j;
//              }
//            }
//            if ($total===$i)
//                $result[] = $total;
//
//            $total = 0;
//        }
//        return $result;
        $i = 1;
        $bool = true;

        while($bool) {

            $i++;
            $mukkamal_son = pow(2, ($i - 1) ) * ( pow(2, $i) - 1);

            if($this->tubsonlikgaTekshirish($i) && $this->tubsonlikgaTekshirish( ( pow(2, $i) - 1) ) ) {
                $result[] = $mukkamal_son;
            }

            if ($mukkamal_son > $N) {
                $bool = false;
            }

        }

        return $result;
    }

    //    6 - masala
    public function problem6(int $x): array
    {
        $results = [];
        $checkIsDivisible=[];
        for ($i=2; $i <=$x; $i++){
            for ($j=2; $j<=$i; $j++){
                if ($i%$j!==0 && $i!==$j) {
                    $checkIsDivisible[$i][$j] = false;
                }else{
                    $checkIsDivisible[$i][$j] = true;
                }
            }
        }

        return $checkIsDivisible;
    }
}