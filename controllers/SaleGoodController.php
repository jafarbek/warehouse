<?php

namespace app\controllers;

use app\models\Good;
use app\models\Sale;
use Yii;
use app\models\SaleGood;
use app\models\SaleGoodSearch;
use yii\base\BaseObject;
use yii\caching\TagDependency;
use yii\helpers\BaseVarDumper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * SaleGoodController implements the CRUD actions for SaleGood model.
 */
class SaleGoodController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SaleGood models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new SaleGoodSearch();
        $sale = Sale::findOne($id);
        $model = new SaleGood();
        if ($model->load(Yii::$app->request->post())){
            if ($model->good->remain->total > $model->amount){
                $model->save();
                Yii::$app->session->setFlash('success','Muvaffaqiyatli saqlandi');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Omborda kerakli miqdor mavjud emas, Qoldiq: '.$model->good->remain[0]->total);
                return $this->refresh();
            }
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('index', [
            'sale'=> $sale,
            'model'=> $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionList()
    {
        $searchModel = new SaleGoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport($good_id)
    {
        $searchModel = new SaleGoodSearch();
        $dataProvider = $searchModel->searchGood(Yii::$app->request->queryParams, $good_id);

        $good = Good::findOne($good_id);
        return $this->render('report', [
            'searchModel' => $searchModel,
            'good'=>$good,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SaleGood model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SaleGood model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SaleGood();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TagDependency::invalidate(Yii::$app->cache,'sale');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SaleGood model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            TagDependency::invalidate(Yii::$app->cache,'sale');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SaleGood model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $good = $this->findModel($id);
        $good->delete();
        return $this->redirect(['index','id'=>$good->sale_id]);
    }

    /**
     * Finds the SaleGood model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SaleGood the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SaleGood::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
