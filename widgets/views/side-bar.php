<?php


/* @var $this \yii\web\View */
/* @var $incomes array */
/* @var $sales array */
?>

<div class="panel panel-footer">
    <div class="panel panel-heading"><?= t("Ombor") ?></div>
    <?= \yii\bootstrap\Nav::widget([
        'options' => ['class' => 'nav nav-pills nav-stacked'],
        'items' => [
            [
                'label' => "Kirimlar",
                'items'=>$incomes,
                'active'=> $action === 'income-good/report'
            ],
            [
                'label' => "Chiqimlar",
                'items'=>$sales,
                'active'=> $action === 'sale-good/report'
            ],
            [
                'label' => "Qoldiq",
                'items'=>$remain,
                'active'=> $action === 'good/remain-good'
            ],
        ]/*$items*/,
        'encodeLabels' => false,
    ]) ?>
</div>
