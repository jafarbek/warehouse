<?php


namespace app\widgets;


use app\models\Good;
use app\models\IncomeGood;
use app\models\SaleGood;
use app\repositories\IncomeGoodRepository;
use app\services\RemainReportService;
use yii\bootstrap\Widget;
use yii\caching\ChainedDependency;
use yii\caching\DbDependency;
use yii\helpers\Html;

class SideBarWidget extends Widget
{
    /** @var Good */
    public $good;
    public $action;

    public function run()
    {
        $this->good = Good::findOne($this->good);
        $remainAll = new RemainReportService();
        $incomes = [];
        $sales = [];
        $remain=[];
    foreach ($remainAll->getReportFromCache() as $goodId => $good) {
        $incomeCount = $good['incomeTotal'];
        $incomeLabel = "<div class='text-left'>{$good['name']}<span class='pull-right'>$incomeCount</span></div>";
        if ($incomeCount > 0) {
            $incomes[] = [
                'label' => $incomeLabel,
                'url' => ['income-good/report', 'good_id' => $goodId],
                'active' => $this->good && $goodId == $this->good->id && $this->action == "income-good/report" ? true : null
            ];
        }
        $salesCount = $good['saleTotal'];
        $salesLabel = "<div class='text-left'>{$good['name']}<span class='pull-right'>$salesCount</span></div>";
        if ($salesCount > 0) {
            $sales[] = [
                'label' => $salesLabel,
                'url' => ['sale-good/report', 'good_id' => $goodId],
                'active' => $this->good && $goodId == $this->good->id && $this->action == "sale-good/report" ? true : null,
            ];
        }

        $remainCount = $good['remain'] ?? 0;
        $remainLabel = "<div class='text-left'>{$good['name']}<span class='pull-right'>$remainCount</span></div>";
        if ($remainCount > 0) {
            $remain[] = [
                'label' => $remainLabel,
                'url' => ['good/remain-good', 'id' => $goodId],
                'active' => $this->good && $goodId == $this->good->id && $this->action == "good/remain-good" ? true : null,
            ];
        }
    }



        return $this->render('side-bar', [
            'incomes' => $incomes,
            'sales' => $sales,
            'remain' => $remain,
            'action' => $this->action
        ]);
    }
}