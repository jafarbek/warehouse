<?php


namespace app\services;

use app\repositories\IncomeGoodRepository;
use app\repositories\SaleGoodRepository;

/**
 * @property IncomeGoodRepository incomeGoodRepository
 * @property SaleGoodRepository saleGoodRepository
 */
class RemainReportService
{
    private IncomeGoodRepository $incomeGoodRepository;
    private SaleGoodRepository $saleGoodRepository;

    public function __construct()
    {
        $this->incomeGoodRepository = new IncomeGoodRepository();
        $this->saleGoodRepository = new SaleGoodRepository();
    }

    public function getAll(): array
    {
        return $this->calculateReport(
            $this->incomeGoodRepository->getAll(),
            $this->saleGoodRepository->getAll()
        );
    }

    public function getReportFromCache(): array
    {
        return $this->calculateReport(
            $this->incomeGoodRepository->getAllFromCache(),
            $this->saleGoodRepository->getAllFromCache()
        );
    }

    public function getTitleAndRemainFromCache(): array
    {
        return $this->calculateRemainAndTitle(
            $this->incomeGoodRepository->getAllFromCache(),
            $this->saleGoodRepository->getAllFromCache()
        );
    }

    public function getByGoodId($goodId): array
    {
        return $this->calculateReportByID(
            $this->incomeGoodRepository->getByGoodID($goodId),
            $this->saleGoodRepository->getByGoodID($goodId)
        );
    }


    private function calculateReport($allIncomes, $allSales): array
    {
        $report = [];
        foreach ($allIncomes as $goodId => $income) {
            $report[$goodId] = [
                'name' => $income['name'],

                /** Kirim bo`lgan tovarlarni hammasi */
                'incomeTotal' => $income['amount'],
                'income_avg' => $income['avg'],
                'income_summery' => $income['summery'],

                /** Chiqim bo`lgan tovarlarni hammasa agar bo'lsa bo'lmasa 0 */
                'saleTotal' => isset($allSales[$goodId]) ? $allSales[$goodId]['amount'] : 0,
                'sale_avg' => isset($allSales[$goodId]) ? $allSales[$goodId]['avg'] : 0,
                'sale_summery' => isset($allSales[$goodId]) ? $allSales[$goodId]['summery'] : 0,

                /** Ombordagi qoldiq */
                'remain' => isset($allSales[$goodId]) ? $income['amount'] - $allSales[$goodId]['amount'] : 0,
                'earn' => isset($allSales[$goodId]) ? ($income['amount'] * $allSales[$goodId]['avg']) - $income['summery'] : 0,
            ];
        }
        return $report;
    }

    private function calculateReportByID($income, $sale): array
    {
        return [
            'name' => $income['name'],

            /** Kirim bo`lgan tovarlarni hammasi */
            'incomeTotal' => $income['amount'],
            'income_avg' => $income['avg'],
            'income_summery' => $income['summery'],

            /** Chiqim bo`lgan tovarlarni hammasi agar bo'lsa bo'lmasa 0 */
            'saleTotal' => isset($sale) ? $sale['amount'] : 0,
            'sale_avg' => isset($sale) ? $sale['avg'] : 0,
            'sale_summery' => isset($sale) ? $sale['summery'] : 0,

            /** Ombordagi qoldiq */
            'remain' => isset($sale) ? $income['amount'] - $sale['amount'] : 0,
            'earn' => isset($sale) ? ($income['amount'] * $sale['avg']) - $income['summery'] : 0,
        ];

    }

    private function calculateRemainAndTitle($allIncomes, $allSales): array
    {
        $list = [];
        foreach ($allIncomes as $goodId => $income) {
            if (isset($allSales[$goodId])
                && (($income['amount'] - $allSales[$goodId]['amount']) > 0)
            ) {
                $list[$goodId] = [
                    'name' => $income['name'],
                    'remain' => isset($allSales[$goodId]) ? $income['amount'] - $allSales[$goodId]['amount'] : 0,
                ];
            }
        }
        return $list;
    }
}